#!/usr/bin/env python3
import connexion
import datetime
import logging
from minizinc import Solver, Instance, Model, MiniZincError
from typing import Dict, Any, Union, List, Optional


logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

# Find the MiniZinc solver configuration for Gecode
gecode = Solver.lookup('gecode')

def to_instance(code) -> Model:
  model = Model()
  model.add_string(code)
  return Instance(gecode, model)


def to_json_type(key, mzn_type: Dict[str, Any], var: Optional[Dict[str, Any]] = None):
  base: Dict[str, Any] = dict(key=key, dim=0)
  base.update(var or {})
  base.update(mzn_type)
  return base


def to_error(e):
  return dict(
    message=e.message,
    raw=e.raw,
    location=dict(line=e.location.line,columns=e.location.columns) if e.location else None,
    type=e.__class__.__name__
  )

def missing_input_error(inputs):
  return dict(
    message='Missing inputs: {0}'.format(', '.join(v['key'] for v in inputs)),
    location=None,
    inputs=inputs,
    type='MissingInput'
  )


def post_analyze(body: Union[str,bytes]) -> Any:
  instance = to_instance(body if isinstance(body, str) else body.decode())
  try:
    meta = instance.analyse()
    vars = meta['vars']
    return dict(
      inputs=[to_json_type(k, v, vars.get(k)) for k, v in meta['input'].items()],
      outputs=[to_json_type(k, v, vars.get(k)) for k, v in meta['output'].items()],
      vars=[to_json_type(k, v) for k, v in vars.items()],
      enums=meta['enums'],
      method=instance.method.name)
  except MiniZincError as e:
    return to_error(e), 400


def solve_impl(model: str, data: Dict[str, Any], **kwargs) -> Any:
  instance = to_instance(model)
  try:
    # verify all the inputs are given
    meta = instance.meta_data
    missing = [to_json_type(k, v) for k, v in meta['input'].items() if k not in data]
    if missing:
      return missing_input_error(missing), 400

    instance.data.update(data)

    # try to solve
    result = instance.solve(**kwargs)

    def to_solution(s):
      return dict(assignments=s.assignments, objective=s.objective)

    vars = meta['vars']
    return dict(
      status=result.status.name,
      complete=result.complete,
      outputs=[to_json_type(k, v, vars.get(k)) for k, v in meta['output'].items()],
      objective=result.objective,
      solutions=[to_solution(s) for s in result]
    )
  except MiniZincError as e:
    return to_error(e), 400


def post_solve(body: Union[str, Dict[str, Union[str, float]]], **kwargs) -> Any:
  if isinstance(body, bytes):
    body = dict(model=body.decode())
  model = body['model']
  data = body.get('data', {})

  for option in ['random_seed', 'all_solutions', 'free_search', 'processes', 'nr_solutions']:
    if option in body:
      kwargs[option] = body[option]

  return solve_impl(model, data, **kwargs)


def post_solve_all(body: Union[str, Dict[str, Union[str, float]]]) -> Any:
  return post_solve(body, all_solutions=True)




app = connexion.FlaskApp(__name__, )
app.add_api('openapi.yaml')
# set the WSGI application callable to allow using uWSGI:
# uwsgi --http :9090 -w app
application = app.app


@app.app.route('/')
def get_index():
  from flask import send_from_directory
  return send_from_directory('./public', 'index.html')


@app.app.route('/<path:path>')
def get_public(path):
  from flask import send_from_directory
  return send_from_directory('./public', path)


if __name__ == '__main__':
    # run our standalone gevent server
    app.run(port=5000, debug=True)
