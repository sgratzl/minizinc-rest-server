minizinc-rest-server
============
[![License: MIT][mit-image]][mit-url] [![CircleCI][ci-image]][ci-url] 


Usage
-----

**TODO**


Development Environment
-----------------------

**Installation**

```bash
git clone https://github.com/sgratzl/minizinc-rest-server.git
cd minizinc-rest-server
```

[mit-image]: https://img.shields.io/badge/License-MIT-yellow.svg
[mit-url]: https://opensource.org/licenses/MIT
[ci-image]: https://circleci.com/gh/sgratzl/minizinc-webide.svg?style=shield
[ci-url]: https://circleci.com/gh/sgratzl/minizinc-webide
