import logging
from minizinc import Solver, Instance, Model


logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

gecode = Solver.lookup('gecode')

model = """
int: n; % The number of queens.

array [1..n] of var 1..n: q;

include "alldifferent.mzn";

constraint alldifferent(q);
constraint alldifferent(i in 1..n)(q[i] + i);
constraint alldifferent(i in 1..n)(q[i] - i);

"""
nqueens = Model()
nqueens.add_string(model)
# Create an Instance of the n-Queens model for Gecode
instance = Instance(gecode, nqueens)
instance.analyse()
# Assign 4 to n
instance["n"] = 4

result = instance.solve(all_solutions=True)

log.info(result)

