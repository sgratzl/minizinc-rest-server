FROM node:8 as client
WORKDIR /data
RUN git clone https://gitlab.com/sgratzl/minizinc-webide.git && \
    cd minizinc-webide && \
    npm install && \
    npm run build:prod


FROM minizinc/minizinc:edge

WORKDIR /data
EXPOSE 80

RUN apk add --no-cache python3 git

COPY --from=client /data/minizinc-webide/build ./public

COPY requirements.txt .

RUN pip3 install --no-cache -r requirements.txt && \
    pip3 install --no-cache gunicorn

COPY app.py openapi.yaml ./
CMD gunicorn --access-logfile - -w 4 -b 0.0.0.0:80 app:application
